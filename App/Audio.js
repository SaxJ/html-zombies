﻿/*
* Author: Saxon Jensen
* Last Modified: 30/01/2012
*/

function audioInit() {

    // Create the Web Audio context
    var context = new webkitAudioContext();

    // Create a bufferSource object to store the audio data
    var source = context.createBufferSource();

    // Create an XHR object to fetch the sound-file from the server
    var request = new XMLHttpRequest();

    // Make an EventListener to handle the sound-file after it has been loaded
    request.addEventListener('load', function (e) {

        // Beginning decoding the audio data from loaded sound-file ...
        context.decodeAudioData(request.response, function (decoded_data) {

            // Store the decoded buffer data in the source object
            source.buffer = decoded_data;

            // Connect the source node to the Web Audio destination node
            source.connect(context.destination);

            // Loop the playback of the source
            source.loop = true;

            // Play back the sound immediately
            source.noteOn(0);

            // Handle any decoding errors
        }, function (e) { console.log(e); }

        // End of decode handler
    );

        // End of Event Listener
    }, false);

    // Point the request to the sound-file that you want to play
    request.open('GET', 'Sounds/danosongs.com-zingara.wav', true);

    // Set the XHR response-type to 'arraybuffer' to store binary data
    request.responseType = "arraybuffer";

    // Begin requesting the sound-file from the server
    request.send();

    // End of init() function
};

// Run the demo when the DOM has finished loading
//document.addEventListener('DOMContentLoaded', function () { audioInit(); }, false);

function startMusic() {
    gameMusic.play();
}

function muteSounds() {
    playAudio = false;
    gameMusic.pause();
}
