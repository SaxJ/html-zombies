﻿/*
* Author: Saxon Jensen
* Last Modified: 30/01/2012
*/
var introStage;
var imageLoadCount = 0;
var toBegin;

/**
* Draws game introduction and title images to the canvas
**/
function drawIntro() {
    var canvas = document.getElementById("gameCanvas");
    var ctx = canvas.getContext('2d');

    introStage = new Stage(canvas);

    var title = new Bitmap("Images/title.png");
    title.x = canvas.width / 2;
    title.y = 100;
    title.regX = 200;
    title.regY = 100;
    introStage.addChild(title);

    toBegin = new Text();
    toBegin.text = "Loading";
    toBegin.font = "20px 'Lucida Console', Monaco, monospace";
    toBegin.textBaseline = "top";
    toBegin.x = canvas.width / 2;
    toBegin.y = 200;
    toBegin.color = "#00FFFF";
    toBegin.textAlign = "center";
    introStage.addChild(toBegin);

    var backgroundImage = new Bitmap("Images/playerFullSize.png");
    backgroundImage.x = canvas.width / 2;
    backgroundImage.y = canvas.height / 2;
    backgroundImage.regX = 200;
    backgroundImage.regY = 200;
    backgroundImage.rotation = -90;
    backgroundImage.alpha = 0.1;
    backgroundImage.scaleX = backgroundImage.scaleY = 2;
    introStage.addChild(backgroundImage);

    var instructionsStart = 220;
    var instructions = [
    "Controls",
    "W, A, D: thrust, turn left, turn right",
    "SPACE: Fire weapon",
    "Z: Turn off audio",
    "P: Pause"
    ]
    
    for (var i = 0; i < instructions.length; i++) {
        var line = new Text();
        line.text = instructions[i];
        line.font = "20px 'Lucida Console', Monaco, monospace";
        line.textBaseline = "top";
        line.x = canvas.width / 2;
        line.y = instructionsStart;
        line.color = "#00FFFF";
        line.textAlign = "center";
        instructionsStart += 25;
        introStage.addChild(line);
    }

    var storyLines = [
            "The year is 2211, and through advanced research into artificial",
            "intelligence, computers have developed their own micro life-forms,",
            "known as vectors",
            "",
            "You are Rhom, the last vector left standing as a virus infects",
            "all the vectors around you, and you fight to protect the last free",
            "computer chip.",
            "Can you save the artificial world, known simply as,",
            "The Plane",
            "",
            "",
            "Free Royalty Free Music by DanoSongs.com"];

    var storyY = instructionsStart;

    for (var i = 0; i < storyLines.length; i++) {
        var storyText = new Text();
        storyText.text = storyLines[i];
        storyText.font = "20px 'Lucida Console', Monaco, monospace";
        storyText.textBaseline = "top";
        storyText.x = canvas.width / 2;
        storyText.y = storyY;
        storyText.color = "#00FFFF";
        storyText.textAlign = "center";
        storyText.shadow = new Shadow("white", 0, 0, 10);
        storyY += 25;
        introStage.addChild(storyText);
    }

    introStage.update();
    startImageLoad();
}

/**
* Preloads images for use in the game. finishImageLoad is called
* upon the load of each image.
**/
function startImageLoad() {
    bulletSprite = new Image();
    bulletSprite.src = "Images/bullet.png";
    bulletSprite.onload = finishImageLoad;

    enemySprite = new Image();
    enemySprite.src = "Images/enemy.png";
    enemySprite.onload = finishImageLoad;

    playerSprite = new Image();
    playerSprite.src = "Images/player.png";
    playerSprite.onload = finishImageLoad;

    cloudSprite = new Image();
    cloudSprite.src = "Images/greenCloud.png";
    cloudSprite.onload = finishImageLoad;

    splatSprite = new Image();
    splatSprite.src = "Images/explosion.png";
    splatSprite.onload = finishImageLoad;

    gridImage = new Image();
    gridImage.src = "Images/grid.png";
    gridImage.onload = finishImageLoad;

    warpSprite = new Image();
    warpSprite.src = "Images/warp.png";
    warpSprite.onload = finishImageLoad;

    plusSprite = new Image();
    plusSprite.src = "Images/plus.png";
    plusSprite.onload = finishImageLoad;

    gunshot = new Audio();
    gunshot.src = "Sounds/37236__shades__gun-pistol-one-shot.wav";

    gameMusic = new Audio();
    gameMusic.src = "Sounds/danosongs.com-zingara.wav";
}

/**
* Counts that all required resources have been loaded, and if so, game state is changed to
* allow the initialisation of the game stage.
**/
function finishImageLoad() {
    imageLoadCount++;
    toBegin.text = "Loading Part " + imageLoadCount + " of 8";

    if (imageLoadCount == 8) {
        toBegin.text = "Press SPACE to begin";
        gameLoaded = true;

        grid = new Bitmap(gridImage);
        grid.x = 0;
        grid.y = 0;
    }

    introStage.update();
}

/**
* Clears the intro stage, and initialises the game stage
**/
function startGame() {
    introStage.removeAllChildren();
    init();
}