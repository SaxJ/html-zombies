A HTML Space Game
=================

A quick and _very_ hacky space game written in Javascript and using HTML 5 canvas for display. Collisions and hit-checking is O(n^2) so it's really quite bad, however this was made very quickly to push out a complete game, rather than making it work well.

I'm writing a new version of this game currently that uses a kd-tree for efficiently checking nearest neighbours, and uses a better object-oriented architecture, rather than the single driving loop seen in this terrible project.