﻿/**
*
* Author: Saxon Jensen
* Last Modified: 26/01/2012
* 
*/

function LineVector(dx, dy) {
    var magnitude = Math.sqrt((dx * dx) + (dy * dy));
    this.dx = dx / magnitude;
    this.dy = dy / magnitude;
}