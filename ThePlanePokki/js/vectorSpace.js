﻿function LineVector(dx, dy) {
    var magnitude = Math.sqrt((dx * dx) + (dy * dy));
    this.dx = dx / magnitude;
    this.dy = dy / magnitude;
}