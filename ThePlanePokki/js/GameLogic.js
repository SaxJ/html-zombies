﻿/*
* Author: Saxon Jensen
* Last Modified: 30/01/2012
*/

/////   CONSTANTS  //////
//player constants
var PLAYER_RADIUS = 20;
var PLAYER_THRUST = 0.25;
var PLAYER_TURN_SPEED = 5;
var PLAYER_SPEED = 8;
var PLAYER_VIEW_DISTANCE = 70;
var REVERSE_THRUST_ENABLED = false;

//bullet constants
var BULLET_RADIUS = 3;
var BULLET_SPEED_FACTOR = 15;
var BULLET_DAMAGE = 10;
var BULLET_SCALE_FACTOR = 1;

//enemy constants
var ENEMY_RADIUS = 15;
var ENEMY_SPEED_FACTOR = 5;
var ENEMY_ROTATION_SPEED = 5;
var ENEMY_SCORE = 3;

//explosion constants
var EXPLOSION_SPIN_SPEED = 30;
var EXPLOSION_RADIUS = 20;
var EXPLOSION_SCALE_SPEED = 0.15;
var EXPLOSION_FADE_SPEED = 0.15;

//used key codes
var KEYCODE_W = 87;
var KEYCODE_A = 65;
var KEYCODE_S = 83;
var KEYCODE_D = 68;
var KEYCODE_SPACE = 32;
var KEYCODE_ENTER = 13;
var KEYCODE_P = 80;
var KEYCODE_Z = 90;
var KEYCODE_Q = 81;

//misc constants
var CLOUD_DELAY_TIME = 4000;
var SPLAT_REMAIN_TIME = 200;
var NUM_PERM_OBJECTS = 3;
var UPGRADE_COST = 100;

//warp constants
var WARP_ENABLED = false;
var WARP_IMAGE_RADIUS = 100;
var WARP_INITIAL_SPIN_SPEED = 35;
var WARP_SPIN_SLOW_RATE = 2;

//register event handlers
document.onkeydown = handleKeyDown;
document.onkeyup = handleKeyUp;

//game object id numbers
var BULLET_ID = 1;
var ENEMY_ID = 2;
var CLOUD_ID = 3;
var SPLAT_ID = 4;
var WARP_ID = 5;
var UPGRADE_ID = 6;

//general game states and variables
var gameStarted = false;
var gameLoaded = false;
var bounds;
var stage;
var healthLabel;
var gameMusic;
var lastDamageTime = 0;
var upgrading = false;
var warpCount = 0;

//movement boolean
var moveLeft;
var moveRight;
var moveUp;
var moveDown;
var turnRight;
var turnLeft;

//permanent object variables
var player;
var mouseX;
var mouseY;
var label;
var bulletVector;
var grid;

//game images
var playerSprite;
var playerMovingSprite;
var bulletSprite;
var enemySprite;
var cloudSprite;
var splatSprite;
var gridImage;
var warpSprite;
var plusSprite;

//score tracking variables
var lastSpawnTime;
var totalKilled;
var waveNumber;
var numberInWave;
var killedInWave;
var lastCloudSpawn = 0;
var totalScore = 0;

//upgrade screen variables
var upgradeTexts;
var upgradeButtons;
var closeUpgradesButton;

//audio variables
var gunshot;
var gameMusic;
var playAudio = true;

/**
* Updates global mouse position variables relative to the
* game canvas.
**/
function getMousePosition(e) {
    var canvas = document.getElementById("gameCanvas");

    if (e.pageX || e.pageY) {
        mouseX = e.pageX;
        mouseY = e.pageY;
    }
    else {
        mouseX = e.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
        mouseY = e.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
    }

    // Convert to coordinates relative to the canvas
    mouseX -= canvas.offsetLeft;
    mouseY -= canvas.offsetTop;
    //console.log("#####################################CLICK (" + mouseX + ", " + mouseY + ")");
    if (upgrading) {
        upgradeAttribute(mouseX, mouseY);
    }
}

/**
* Returns the angle in degrees to the mouse from the player.
* @returns Angle in degrees from player to mouse.
**/
function angleToMouse() {
    var diffY = (mouseY - player.y);
    var diffX = (mouseX - player.x);

    bulletVector = new LineVector(diffX, diffY);

    return (Math.atan2(diffY, diffX) * (180 / Math.PI));
}

/**
* Returns the angle in degrees of the player from the specified co-ordinates.
* @param cx The x co-ordinate
* @param cy The y co-ordinate
* @returns The angle in degrees to the player
**/
function angleToPlayer(cx, cy) {
    var dx = player.x - cx;
    var dy = player.y - cy;

    vector = new LineVector(dx, dy);
    return (Math.atan2(dy, dx) * (180 / Math.PI));
}


/**
* Returns the distance from the specified co-ordinates to the player.
**/
function distanceToPlayer(x, y) {
    var dx = x - player.x;
    var dy = y - player.y;
    return Math.sqrt((dx * dx) + (dy * dy));
}

/**
* Initializes game variables and game state, adding required display
* objects to the canvas.
**/
function init() {
    totalKilled = 0;
    waveNumber = 1;
    numberInWave = 20 * waveNumber;
    killedInWave = 0;
    startMusic();

    var canvas = document.getElementById("gameCanvas");
    var ctx = canvas.getContext('2d');
    canvas.addEventListener('click', getMousePosition, false); //added

    bounds = new Rectangle();
    bounds.width = canvas.width;
    bounds.height = canvas.height;

    moveLeft = false;
    moveRight = false;
    moveDown = false;
    moveUp = false;
    turnLeft = false;
    turnRight = false;

    stage = new Stage(canvas);

    var g = new Graphics();
    g.setStrokeStyle(2);
    g.beginStroke(Graphics.getRGB(255, 255, 255, 1));
    g.drawCircle(0, 0, PLAYER_RADIUS);
    g.drawRect(0, -1, 30, 2);

    //initialise player
    player = new Bitmap(playerSprite);
    player.x = bounds.width / 2;
    player.y = bounds.height / 2;
    player.regX = PLAYER_RADIUS;
    player.regY = PLAYER_RADIUS;
    player.rotation = 10;

    player.vx = 0;
    player.vy = 0;
    player.ax = 0;
    player.ay = 0;

    player.alive = true;
    player.health = 100;
    player.onclick = function(e) {
        alert("bob");
    }

    //initialise the current score and health label
    healthLabel = new Text();
    healthLabel.text = player.health + "% | Wave: " + waveNumber + " | " + killedInWave + " / " + numberInWave + " | Total: " + totalKilled;
    healthLabel.textBaseline = "top";
    healthLabel.x = 20;
    healthLabel.y = 20;
    healthLabel.color = "#00FFFF";
    healthLabel.font = "bold 20px 'Lucida Console', Monaco, monospace";

    createUpgradeScreenObjects();

    stage.addChild(grid);
    stage.addChild(player);
    stage.addChild(healthLabel);
    stage.mouseEventsEnabled = true;

    Ticker.setFPS(30);
    Ticker.addListener(this);
    Ticker.setPaused(false);

    //set locally stored score if it does not exist already
    spawnEnemy();
    lastSpawnTime = Ticker.getTime();
    //console.log(localStorage.highScore);
    if (!localStorage.highScore) {
        localStorage.highScore = 0;
    }

    stage.update();
}

/**
* Handler for the key down event, modifying movement variables
**/
function handleKeyDown(e) {
    if (!e) {
        var e = window.event;
    }
    switch (e.keyCode) {
        case KEYCODE_A:
            moveLeft = true;
            turnLeft = true;
            break;
        case KEYCODE_S:
            moveDown = true;
            break;
        case KEYCODE_D:
            moveRight = true;
            turnRight = true;
            break;
        case KEYCODE_W:
            moveUp = true;
            break;
        case KEYCODE_SPACE:
            if (!gameStarted && gameLoaded) {
                startGame();
                gameStarted = true;
            } else {
                spawnBullet();
            }
            break;
        case KEYCODE_P:
            healthLabel.text = "PAUSED";
            stage.update();
            Ticker.setPaused(!Ticker.getPaused());
            break;
        case KEYCODE_Z:
            playAudio = !playAudio;
            if (!playAudio) {
                gameMusic.pause();
            } else {
                gameMusic.play();
            }
            break;
        case KEYCODE_Q:
            if (WARP_ENABLED) {
                spawnWarp();
            }
            break;
    }
}

/**
* Handler for the key up event
**/
function handleKeyUp(e) {
    if (!e) {
        e = window.event;
    }

    switch (e.keyCode) {
        case KEYCODE_A:
            moveLeft = false;
            turnLeft = false;
            break;
        case KEYCODE_D:
            moveRight = false;
            turnRight = false;
            break;
        case KEYCODE_S:
            moveDown = false;
            break;
        case KEYCODE_W:
            moveUp = false;
            break;
    }
}

/**
* Updated player position and rotation
**/
function updatePlayer() {
    if (turnLeft) {
        player.rotation -= PLAYER_TURN_SPEED;
    }

    if (turnRight) {
        console.log("ROTATE RIGHT");
        player.rotation += PLAYER_TURN_SPEED;
    }

    if (moveUp) {
        player.vx += Math.cos((Math.PI / 180) * player.rotation) * PLAYER_THRUST;
        player.vy += Math.sin((Math.PI / 180) * player.rotation) * PLAYER_THRUST;
    }

    if (moveDown && REVERSE_THRUST_ENABLED) {
        player.vx += Math.cos((Math.PI / 180) * player.rotation) * PLAYER_THRUST * -1;
        player.vy += Math.sin((Math.PI / 180) * player.rotation) * PLAYER_THRUST * -1;
    }

    player.x += player.vx;
    player.y += player.vy;
    if (player.x > bounds.width - PLAYER_RADIUS) {
        player.x -= player.vx;
        player.vx = 0;
    }
    if (player.x < PLAYER_RADIUS) {
        player.x += player.vx;
        player.vx = 0;
    }
    if (player.y > bounds.height - PLAYER_RADIUS) {
        player.y -= player.vy;
        player.vy = 0;
    }
    if (player.y < PLAYER_RADIUS) {
        player.y += player.vy;
        player.vy = 0;
    }
}


/**
* Called repetedly as specified by Ticker.setFPS.
* Calls functions to update the state of each display object.
**/
function tick() {
    updatePlayer();
    updateBullets();
    updateEnemy();
    detectPlayerEnemyCollision();
    detectBulletEnemyCollision();
    detectEnemyEnemyCollision();
    updateClouds();
    updateSplats();
    updateWarp();

    healthLabel.text = player.health + "% | Wave: " + waveNumber + " | " + killedInWave + " / " + numberInWave + " | Total Kills: " + totalKilled + " | Score: " + totalScore;

    numberInWave = 20 * waveNumber;
    if (killedInWave == numberInWave) {
        waveNumber++;
        killedInWave = 0;
        displayUpgrades();
    }

    if ((Ticker.getTime() - lastSpawnTime) > (1000 + (2000 * waveNumber))) {
        //var n = Math.pow(Math.round(waveNumber / 2), 3);
        for (var i = 0; i < waveNumber; i++) {
            spawnEnemy();
        }
        lastSpawnTime = Ticker.getTime();
    }

    if (player.health < 0) {
        gameEnd();
    }

    if ((Ticker.getTime() - lastCloudSpawn) > CLOUD_DELAY_TIME) {
        spawnCloud();
        lastCloudSpawn = Ticker.getTime();
    }

    stage.update();
}

/**
* Creates a display object representing a bullet, initialises it
* to match the angle of the player, and adds it to the stage.
**/
function spawnBullet() {
    var g = new Graphics();
    g.setStrokeStyle(2);
    g.beginStroke(Graphics.getRGB(255, 255, 255, 1));
    g.drawCircle(0, 0, BULLET_RADIUS);

    //var bullet = new Shape(g);
    var bullet = new Bitmap(bulletSprite);
    bullet.x = player.x;
    bullet.y = player.y;
    bullet.active = true;
    bullet.dx = Math.cos((Math.PI / 180) * player.rotation);
    bullet.dy = Math.sin((Math.PI / 180) * player.rotation);
    bullet.rotation = player.rotation;
    bullet.shadow = new Shadow("green", 0, 0, 13);
    bullet.scaleY = BULLET_SCALE_FACTOR;

    bullet.id = BULLET_ID;
    if (playAudio) {
        gunshot.play();
    }
    stage.addChild(bullet);
}

/**
* Updates position of bullets on stage
**/
function updateBullets() {
    for (var i = NUM_PERM_OBJECTS; i < stage.getNumChildren(); i++) {
        var bullet = stage.getChildAt(i);

        if (bullet.id != BULLET_ID) {
            continue;
        }

        if (bullet == -1 || bullet == null) {
            return;
        }

        bullet.x += (bullet.dx * BULLET_SPEED_FACTOR);
        bullet.y += (bullet.dy * BULLET_SPEED_FACTOR);
        bullet.alpha = 100 / (2 * distanceToPlayer(bullet.x, bullet.y));
        //console.log("Bullet y speed = " + bullet.dy * BULLET_SPEED_FACTOR);
        //console.log("Bullet x speed = " + bullet.dx * BULLET_SPEED_FACTOR);

        if (bullet.x > bounds.width || bullet.x < 0) {
            stage.removeChild(bullet);
        }
        if (bullet.y > bounds.height || bullet.y < 0) {
            stage.removeChild(bullet);
        }
        //console.log("BULLET ID = " + bullet.id);
    }
}


/**
* Returns random number between the specified numbers inclusive
**/
function randomInt(min, max) {
    return Math.random() * (max - min) + min;
}

/**
* Randomly selects location on stage borders for enemy spawn
**/
function selectEnemySpawnLocation(enemy) {
    var side = Math.round(randomInt(1, 4));
    //console.log("SIDE = " + side);
    switch (side) {
        case 1:
            enemy.x = randomInt(0, bounds.width);
            enemy.y = -20;
            break;
        case 2:
            enemy.x = bounds.width + 20;
            enemy.y = randomInt(0, bounds.height);
            break;
        case 3:
            enemy.x = randomInt(0, bounds.width);
            enemy.y = bounds.height + 20;
            break;
        case 4:
            enemy.x = -20;
            enemy.y = randomInt(0, bounds.height);
            break;
    }
}

/**
* Initialises enemy object and adds it to the stage
**/
function spawnEnemy() {
    var g = new Graphics();
    g.setStrokeStyle(2);
    g.beginStroke(Graphics.getRGB(255, 0, 0, 1));
    g.beginFill(Graphics.getRGB(255, 0, 0, 1));
    g.drawCircle(0, 0, ENEMY_RADIUS);

    //var enemy = new Shape(g);
    var enemy = new Bitmap(enemySprite);
    selectEnemySpawnLocation(enemy);
    enemy.id = ENEMY_ID;
    enemy.canMove = true;
    enemy.radius = ENEMY_RADIUS;
    enemy.health = 10;
    enemy.regX = ENEMY_RADIUS;
    enemy.regY = ENEMY_RADIUS;

    stage.addChild(enemy);
}

/**
* Updates enemy positions
**/
function updateEnemy() {
    for (var i = NUM_PERM_OBJECTS; i < stage.getNumChildren(); i++) {
        var enemy = stage.getChildAt(i);

        if (enemy.id != ENEMY_ID ) {
            continue;
        }

        var dx = player.x - enemy.x;
        var dy = player.y - enemy.y;
        var vector = new LineVector(dx, dy);

        if (enemy.canMove) {
            enemy.x += vector.dx * ENEMY_SPEED_FACTOR;
            enemy.y += vector.dy * ENEMY_SPEED_FACTOR;
        }
        enemy.rotation += ENEMY_ROTATION_SPEED;

        if (enemy.health <= 0) {
            spawnSplat(enemy);
            stage.removeChildAt(i);
            totalKilled++;
            killedInWave++;
            totalScore += waveNumber * ENEMY_SCORE;
        }
    }
}

/**
* Rounds num to the specified number of decimal placed (dec)
**/
function roundNumber(num, dec) {
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}

/**
* Controls interaction between display objects representing an enemy and the player
**/
function detectPlayerEnemyCollision() {
    for (var i = NUM_PERM_OBJECTS; i < stage.getNumChildren(); i++) {
        var enemy = stage.getChildAt(i);

        if (enemy.id != ENEMY_ID) {
            continue;
        }

        var dx = enemy.x - player.x;
        var dy = enemy.y - player.y;
        var distance = Math.sqrt((dx * dx) + (dy * dy));

        if (distance < (PLAYER_RADIUS + enemy.radius)) {
            if ((Ticker.getTime() - lastDamageTime) > 1000) {
                player.health -= enemy.health;
                lastDamageTime = Ticker.getTime();
            }
            enemy.canMove = false;
        } else {
            enemy.canMove = true;
        }
    }
}

/**
* Manages and detects collisions between a bullet and an enemy object
**/
function detectBulletEnemyCollision() {
    for (var b = NUM_PERM_OBJECTS; b < stage.getNumChildren(); b++) {
        var bullet = stage.getChildAt(b);
        if (bullet.id != BULLET_ID) {
            continue;
        }

        for (var e = NUM_PERM_OBJECTS; e < stage.getNumChildren(); e++) {
            var enemy = stage.getChildAt(e);

            if (enemy.id != ENEMY_ID) {
                continue;
            }

            var dx = enemy.x - bullet.x;
            var dy = enemy.y - bullet.y;
            var distance = Math.sqrt((dx * dx) + (dy * dy));

            if (distance < (BULLET_RADIUS + enemy.radius)) {
                stage.removeChildAt(b);
                enemy.health -= BULLET_DAMAGE;
            }
        }
    }
}

/**
* Manages and detects he collision of 2 enemy objects
**/
function detectEnemyEnemyCollision() {
    for (var a = NUM_PERM_OBJECTS; a < stage.getNumChildren(); a++) {
        var enemyA = stage.getChildAt(a);

        if (enemyA.id != ENEMY_ID) {
            continue;
        }
        for (var b = NUM_PERM_OBJECTS; b < stage.getNumChildren(); b++) {
            var enemyB = stage.getChildAt(b);

            if (enemyB.id != ENEMY_ID || a == b) {
                continue;
            }

            var dx = enemyA.x - enemyB.x;
            var dy = enemyA.y - enemyB.y;
            var distance = Math.sqrt((dx * dx) + (dy * dy));

            if (distance < (enemyA.radius + enemyB.radius)) {
                if (enemyA.radius + enemyB.radius < 32) {
                    combineEnemies(enemyA, enemyB);
                }
            }
        }
    }
}

/**
* Merges to enemies a and b together into one enemy object, having 
* the parameters of the two previous objects combined.
**/
function combineEnemies(a, b) {
    //console.log("COMBINE CALLED");
    var newRadius = a.radius + b.radius;
    //console.log("a radius = " + a.radius);

    var g = new Graphics();
    g.setStrokeStyle(2);
    g.beginStroke(Graphics.getRGB(255, 0, 0, 1));
    g.beginFill(Graphics.getRGB(255, 0, 0, 1));
    g.drawCircle(0, 0, newRadius);

    //enemy = new Shape(g);
    enemy = new Bitmap(enemySprite);
    enemy.x = a.x;
    enemy.y = a.y;
    enemy.id = ENEMY_ID;
    enemy.canMove = true;
    enemy.radius = newRadius;
    enemy.health = a.health + b.health;

    var factor = newRadius / ENEMY_RADIUS;
    enemy.scaleX = factor;
    enemy.scaleY = factor;
    enemy.regX = newRadius / 2;
    enemy.regY = newRadius / 2;
    enemy.alpha = a.alpha;
    enemy.rotation = a.rotation;


    stage.addChild(enemy);
    stage.removeChild(a);
    stage.removeChild(b);
}

/**
* Wipes the canvas and finalises the game score on player death
**/
function gameEnd() {
    gameStarted = false;
    //Ticker.setPaused(true);
    stage.removeAllChildren();

    var deathLabel = new Text();
    deathLabel.text = "You Died";
    deathLabel.textAlign = "center";
    deathLabel.textBaseline = "top";
    deathLabel.x = bounds.width / 2;
    deathLabel.y = 10;
    deathLabel.color = "#00FFFF";
    deathLabel.font = "30px 'Lucida Console', Monaco, monospace";

    var scoreLabel = new Text();
    scoreLabel.textAlign = "center";
    scoreLabel.textBaseline = "top";
    scoreLabel.x = bounds.width / 2;
    scoreLabel.y = 50;
    scoreLabel.color = "#00FFFF";
    scoreLabel.font = "30px 'Lucida Console', Monaco, monospace";
    if (totalScore > localStorage.highScore) {
        localStorage.highScore = totalScore;
        scoreLabel.text = "New Highscore: " + totalScore;
    } else {
        scoreLabel.text = "Highscore Unbeaten: " + localStorage.highScore;
    }

    stage.addChild(deathLabel);
    stage.addChild(scoreLabel);
}

/**
* Spawns a randomly scaled and alpha cloud bitmap object
**/
function spawnCloud() {
    var cloud = new Bitmap(cloudSprite);
    cloud.x = -600;
    cloud.y = randomInt(-50, bounds.height - 20);
    cloud.speed = Math.round(randomInt(1, 3));
    cloud.alpha = Math.random();
    cloud.id = CLOUD_ID;
    cloud.scaleX = randomInt(1, 3);
    cloud.scaleY = randomInt(1, 3);
    stage.addChild(cloud);
}

/**
* Updates cloud positions so they appear to drift across stage
**/
function updateClouds() {
    for (var i = NUM_PERM_OBJECTS; i < stage.getNumChildren(); i++) {
        var cloud = stage.getChildAt(i);

        if (cloud.id != CLOUD_ID) {
            continue;
        }

        cloud.x += cloud.speed;

        if (cloud.x >= bounds.width) {
            stage.removeChildAt(i);
        }
    }
}

/**
* Spawns an explosion effect bitmap at the location of an
* enemy death.
**/
function spawnSplat(enemy) {
    var splat = new Bitmap(splatSprite);
    splat.x = enemy.x;
    splat.y = enemy.y;
    splat.id = SPLAT_ID;
    splat.regX = EXPLOSION_RADIUS;
    splat.regY = EXPLOSION_RADIUS;

    var scaleFactor = enemy.radius / ENEMY_RADIUS;
    splat.scaleX = scaleFactor;
    splat.scaleY = scaleFactor;
    splat.spawnTime = Ticker.getTime();
    stage.addChildAt(splat, stage.getNumChildren());
}

/**
* Updates explosion bitmap to spin and expand then fade
**/
function updateSplats() {
    for (var i = NUM_PERM_OBJECTS; i < stage.getNumChildren(); i++) {
        var s = stage.getChildAt(i);

        if (s.id != SPLAT_ID) {
            continue;
        }

        s.rotation += EXPLOSION_SPIN_SPEED;
        s.scaleX += EXPLOSION_SCALE_SPEED;
        s.scaleY += EXPLOSION_SCALE_SPEED;
        s.alpha -= EXPLOSION_FADE_SPEED;

        if (s.alpha <= 0) {
            stage.removeChildAt(i);
        }

    }
}

function displayUpgrades() {
    Ticker.setPaused(true);
    upgrading = true;

    for (var i = 0; i < upgradeButtons.length; i++) {
        stage.addChild(upgradeButtons[i]);
        stage.addChild(upgradeTexts[i]);
    }
    stage.addChild(closeUpgradesButton);
    stage.update();
}

function upgradeAttribute(x, y) {
    for (var i = 0; i < 5; i++) {
        var b = upgradeButtons[i];
        if (x > b.x && x < (b.x + b.width) && y > b.y && y < (b.y + b.height)) {
            upgradeValue(i + 1);
        }
    }
    if (x > closeUpgradesButton.x && x < (closeUpgradesButton.x + 200) && y > closeUpgradesButton.y && y < (closeUpgradesButton.y + 50)) {
        closeUpgrade();
    }
}

function closeUpgrade() {
    Ticker.setPaused(false);
    upgrading = false;
    for (var i = 0; i < 5; i++) {
        stage.removeChild(upgradeButtons[i]);
        stage.removeChild(upgradeTexts[i]);
        stage.removeChild(closeUpgradesButton);
    }
}

function upgradeValue(n) {
    //console.log("UPGRADE VALUE " + n);
    if (totalScore < UPGRADE_COST) {
        return;
    }
    switch (n) {
        case 1:
            PLAYER_TURN_SPEED += 1;
            upgradeTexts[0].text = "Rotation Engines:" + PLAYER_TURN_SPEED;
            break;
        case 2:
            PLAYER_THRUST += 0.05;
            upgradeTexts[1].text = "Main Engines: " + PLAYER_THRUST;
            break;
        case 3:
            BULLET_SCALE_FACTOR += 0.2;
            BULLET_RADIUS += BULLET_RADIUS * 0.2;
            upgradeTexts[2].text = "Ammunition Diameter: " + BULLET_SCALE_FACTOR;
            break;
        case 4:
            WARP_ENABLED = true;
            warpCount += 3;
            upgradeTexts[3].text = "Warp Drive: " + WARP_ENABLED + "(3 added to stock)";
            break;
        case 5:
            REVERSE_THRUST_ENABLED = true;
            upgradeTexts[4].text = "Reverse Thrusters: " + REVERSE_THRUST_ENABLED;
            break;
    }
    totalScore -= UPGRADE_COST;
    stage.update();
}

function spawnWarp() {
    if (warpCount <= 0) {
        return;
    }
    warpCount--;
    var warp = new Bitmap(warpSprite);
    warp.x = player.x;
    warp.y = player.y;
    warp.regX = WARP_IMAGE_RADIUS;
    warp.regY = WARP_IMAGE_RADIUS;
    warp.scaleX = warp.scaleY = 0.5;
    warp.radius = warp.scaleX * WARP_IMAGE_RADIUS;
    warp.id = WARP_ID;
    warp.spinSpeed = WARP_INITIAL_SPIN_SPEED;
    stage.addChild(warp);
}

function updateWarp() {
    for (var i = NUM_PERM_OBJECTS; i < stage.getNumChildren(); i++) {
        var warp = stage.getChildAt(i);
        if (warp.id != WARP_ID) {
            continue;
        }

        warp.rotation += warp.spinSpeed;
        warp.spinSpeed -= WARP_SPIN_SLOW_RATE;
        warp.alpha -= 0.025;
        warp.scaleX = warp.scaleY += 0.1;
        warp.radius = warp.scaleX * WARP_IMAGE_RADIUS;

        for (var n = NUM_PERM_OBJECTS; n < stage.getNumChildren(); n++) {
            var enemy = stage.getChildAt(n);
            if (enemy.id != ENEMY_ID) {
                continue;
            }

            var distance = distanceToPlayer(enemy.x, enemy.y);
            if (distance < (warp.radius + enemy.radius)) {
                spawnSplat(enemy);
                stage.removeChildAt(n);
                totalKilled++;
                killedInWave++;
                totalScore += waveNumber * ENEMY_SCORE;
            }
        }

        if (warp.alpha <= 0) {
            stage.removeChildAt(i);
        }
    }
}

function createUpgradeScreenObjects() {
    upgradeTexts = new Array();
    for (var i = 0; i < 5; i++) {
        upgradeTexts[i] = new Text();
        upgradeTexts[i].textAlign = "left";
        upgradeTexts[i].textBaseline = "top";
        upgradeTexts[i].x = bounds.width / 5;
        upgradeTexts[i].y = 50 + 50 * (i + 1);
        upgradeTexts[i].color = "#00FFFF";
        upgradeTexts[i].font = "20px 'Lucida Console', Monaco, monospace";
    }
    upgradeTexts[0].text = "Rotation Engines:" + PLAYER_TURN_SPEED;
    upgradeTexts[1].text = "Main Engines: " + PLAYER_THRUST;
    upgradeTexts[2].text = "Ammunition Diameter: " + BULLET_SCALE_FACTOR;
    upgradeTexts[3].text = "Warp Drive: " + WARP_ENABLED;
    upgradeTexts[4].text = "Reverse Thrusters: " + REVERSE_THRUST_ENABLED;

    upgradeButtons = new Array();
    for (var i = 0; i < 5; i++) {
        upgradeButtons[i] = new Bitmap(plusSprite);
        upgradeButtons[i].x = 4 * bounds.width / 5;
        upgradeButtons[i].y = 45 + 50 * (i + 1);
        upgradeButtons[i].width = upgradeButtons[i].height = 40;
    }

    closeUpgradesButton = new Text();
    closeUpgradesButton.text = "DONE";
    closeUpgradesButton.textAlign = "center";
    closeUpgradesButton.textBaseline = "top";
    closeUpgradesButton.x = bounds.width / 2;
    closeUpgradesButton.y = bounds.height - 60;
    closeUpgradesButton.color = "#00FFFF";
    closeUpgradesButton.font = "Bold 50px Impact";
}